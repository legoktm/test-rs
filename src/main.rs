//! test-rs

#[tokio::main]
async fn main() {
    println!("hi!!");
    let text = "".to_string();
    #[cfg(feature = "a")]
    let text = format!("{} a", text);
    #[cfg(feature = "b")]
    let text = format!("{} b", text);
    println!("the following features are enabled: {}", text);
}
